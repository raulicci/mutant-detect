from api.dna import DNA
from urllib import request
# Create your views here.
from api.models import Stat
from rest_framework import viewsets, permissions, status
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from api.serializers import StatsSerializer, MutationSerializer
from rest_framework import serializers
from django.views.decorators.cache import cache_page


class MutationViewSet(GenericViewSet):
    """
    Stat ViewSet
    """

    serializer_class = MutationSerializer
    permission_classes = [permissions.IsAuthenticated]
    #queryset = Stat.objects.all()
     
    def is_mutant(self, request):
        print(request.data)
        """
        Verifies if a human is or not a mutant, saving the result.
        """
        serializer = MutationSerializer(data=request.data)
        if serializer.is_valid():
            dna = serializer.data["dna"]
            print(dna)
            stat = DNA.isMutant(dna)
            print(stat)
            entry = Stat.objects.filter(dna=dna)
            if len(entry) > 0:
                print('no se guarda, porque ya existe')
                pass
            else:
                static = Stat()
                static.dna = dna
                static.resultado = stat
                static.save()
            return Response(status= \
                status.HTTP_200_OK if stat else status.HTTP_403_FORBIDDEN \
            )

        # I would have preferred to return a HTTP_400_BAD_REQUEST, but the challenge said 403
        return Response(status=status.HTTP_403_FORBIDDEN)

class StatsViewSet(GenericViewSet):

    """
    API endpoint that allows groups to be viewed or edited.
    """

    serializer_class = StatsSerializer
    permission_classes = [permissions.AllowAny]

    def get_stats(self):
            """
            Gets the humans vs mutants stats.
            """
            humans = Stat.objects.all()

            total = humans.count()
            count_mutant_dna = count_human_dna = ratio = 0

            if total > 0:
                for human in humans:
                    count_mutant_dna += 1 if human.resultado else 0
                count_human_dna = total - count_mutant_dna

                # For me the right formula is count_mutant_dna / total
                ratio = (count_mutant_dna / count_human_dna if count_human_dna > 0 else 0)
            
            print(count_mutant_dna)
            print(count_human_dna)
            print(ratio)
            return { \
                'count_mutantions': count_mutant_dna, \
                'count_no_mutation': count_human_dna, \
                'ratio': ratio \
            }

    def Statics(self, request):
        stats = self.get_stats()

        serializer = StatsSerializer(stats)


        return Response(serializer.data, status=status.HTTP_200_OK)