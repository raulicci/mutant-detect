from django.db import models

# Create your models here.
class Stat(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    dna = models.TextField(max_length=100, blank=False, default='', unique=True)
    resultado = models.BooleanField(default=False, verbose_name='ADN contiene mutacion')
 
    class Meta:
        ordering = ['fecha']

    def __str__(self):
        return self.dna