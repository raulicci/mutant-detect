"""mutandetect URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers
from api.views import MutationViewSet, StatsViewSet
from api import views

router = routers.DefaultRouter()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('stats/', views.StatsViewSet.as_view({'get': 'Statics'}), name='Stats'),
    path('mutation/', views.MutationViewSet.as_view({'post': 'is_mutant'}), name='Mutation'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]